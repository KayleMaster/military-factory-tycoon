///itemMove();
//Тази функция е същия код от obj_item > End of Path, но просто във функция

gridx = floor(x/64)*64;
gridy = floor(y/64)*64;

switch(path)
{
 //---------------------------------------------------------------------------------------------------\\ 
  case straight_path_right: // ако вървиме надясно
      if instance_position(gridx+1,gridy,obj_conveyor) //ако има конвейор от дясно
        { 
          switch(instance_position(gridx+1,gridy,obj_conveyor).conveyor_type) // и какъв е вида на конвейора
           {
            case right: // тука сочи надясно
             path_start(straight_path_right,1,0,0);
             path = straight_path_right;
             exit;
            break;
            
            case down: //ако конвейора в който влиза сочи на долу 
              switch(position)//не отново...  позицията на предмета в конвейора, в коя "лента" е
               {
                case "center": // ако предмета се намира в центъра на конвейора
                  if not collision_rectangle(x+31,y-14,x+16,y+14,obj_item,0,1)//ако няма обект obj_item на конвейора в който влиза 
                   {
                        x+=16; 
                        path_start(straight_path_down_half,1,0,0);
                        path = straight_path_down;  
                        position = "right"; // вече се намира от дясната страна
                        exit; //излизаме от функцията (scripta) itemMove() защото направихме квото треа, няма нужда да проверяваме нататък
                   }   else {alarm[0] = 1; exit;} // еми няма място брат, изчакай малко да се освободи място на конвейора и провери пак след 1 стъпка
                break;
                
                case "right":  // ако се намира от дясната му страна (по посока на движението)
                  if not collision_rectangle(x+31,y-14+16,x+16,y+14+16,obj_item,0,1)//ако няма обект obj_item на конвейора в който влиза 
                   {
                        x+=16; 
                        path_start(straight_path_down_one_third,1,0,0); //е тука вече по скайпа ще ти обяснявам :Д
                        path = straight_path_down; 
                        position = "right";// вече се намира от дясната страна
                        exit; 
                   } else {alarm[0] = 1; exit;}                   
                break;
                
                case "left":   // и ако се намира от лявата страна
                  if not collision_rectangle(x+31,y-14-16,x+16,y+14-16,obj_item,0,1)//ако няма обект obj_item на конвейора в който влиза 
                   {
                        x+=16; 
                        path_start(straight_path_down_two_third,1,0,0); //е тука вече по скайпа ще ти обяснявам :Д
                        path = straight_path_down; 
                        position = "right"; // вече се намира от дясната страна
                        exit; 
                   }   else {alarm[0] = 1; exit;}                         
                break;
               }
             
            
            
            case up: //айде отново, ако конвейора в който влиза сочи на горе
             switch(position)
              {
               case "center":
                 if not collision_rectangle(x+31,y-14,x+16,y+14,obj_item,0,1)//ако няма обект obj_item на конвейора в който влиза         
                   {
                     x+=16; 
                     path_start(straight_path_up_half,1,0,0);
                     path = straight_path_up; 
                     position = "left"; // този сочи нагоре значи ще е от лявата му страна
                     exit; 
                   } else {alarm[0]=1; exit;} //няма място, навивам ти аларма да провериш пак след 1 стъпка (1 стъпка = 1 fps)                              
               break;
               
               case "right":
                 if not collision_rectangle(x+31,y-14+16,x+16,y+14+16,obj_item,0,1)//ако няма обект obj_item на конвейора в който влиза         
                   {
                     x+=16; 
                     path_start(straight_path_up_two_third,1,0,0);
                     path = straight_path_up; 
                     position = "left"; // този сочи нагоре значи ще е от лявата му страна
                     exit; 
                   } else {alarm[0]=1; exit;}                    
               break;
               
               case "left":
                 if not collision_rectangle(x+31,y-14-16,x+16,y+14-16,obj_item,0,1)//ако няма обект obj_item на конвейора в който влиза         
                   {
                     x+=16; 
                     path_start(straight_path_up_one_third,1,0,0);
                     path = straight_path_up; 
                     position = "left"; // този сочи нагоре значи ще е от лявата му страна
                     exit; 
                   } else {alarm[0]=1; exit;}                    
               break; 
              }
            break; 
            
            default:
               alarm[0]=1; exit;
            break;            
           }
         }else{alarm[0]=1; exit;} // не че няма място, ама то няма и конвейор хех, изчакай докато някой ти построй такъв
  break;
 //---------------------------------------------------------------------------------------------------\\ 
    case straight_path_left: 
      if instance_position(gridx-1,gridy,obj_conveyor)
        { 
          switch(instance_position(gridx-1,gridy,obj_conveyor).conveyor_type)
             {
               case left:
                 path_start(straight_path_left,1,0,0);
                 path = straight_path_left;
                 exit;
               break;
             
                                                          
               case down: 
                 switch(position)
                  {
                   case "center":
                     if not collision_rectangle(x-31,y-14,x-16,y+14,obj_item,0,1)//ако няма обект obj_item на конвейора в който влиза         
                      {
                       x-=16;
                       path_start(straight_path_down_half,1,0,0);       
                       path = straight_path_down;  
                       position = "left";  
                       exit;
                      } else {alarm[0]=1; exit;}                    
                   break;
                   
                   case "right":
                     if not collision_rectangle(x-31,y-14-16,x-16,y+14-16,obj_item,0,1)//ако няма обект obj_item на конвейора в който влиза         
                      {
                       x-=16;
                       path_start(straight_path_down_two_third,1,0,0);       
                       path = straight_path_down;   
                       position = "left";
                       exit;   
                      } else {alarm[0]=1; exit;}                      
                   break;
                   
                   case "left":
                     if not collision_rectangle(x-31,y-14+16,x-16,y+14+16,obj_item,0,1)//ако няма обект obj_item на конвейора в който влиза         
                      {
                       x-=16;
                       path_start(straight_path_down_one_third,1,0,0);       
                       path = straight_path_down;   
                       position = "left";
                       exit;   
                      } else {alarm[0]=1; exit;}                      
                   break;                   
                  }                     
               break;                
             
                                                         
               case up:
                 switch(position)
                  {
                   case "center":
                    if not collision_rectangle(x-31,y-14,x-16,y+14,obj_item,0,1)       
                     {
                      x-=16;
                      path_start(straight_path_up_half,1,0,0);       
                      path = straight_path_up;
                      position = "right";
                      exit;    
                     } else {alarm[0]=1;exit;}                      
                   break;
                   
                   case "left":
                    if not collision_rectangle(x-31,y-14+16,x-16,y+14+16,obj_item,0,1)       
                     {
                      x-=16;
                      path_start(straight_path_up_two_third,1,0,0);       
                      path = straight_path_up;
                      position = "right";
                      exit;    
                     } else {alarm[0]=1;exit;}                      
                   break;
                   
                   case "right":
                    if not collision_rectangle(x-31,y-14-16,x-16,y+14-16,obj_item,0,1)       
                     {
                      x-=16;
                      path_start(straight_path_up_one_third,1,0,0);       
                      path = straight_path_up;
                      position = "right";
                      exit;    
                     } else {alarm[0]=1;exit;}                      
                   break;
                  }
  
               break;
               
               default:
               alarm[0]=1; exit;
               break;
         } 
      }else{alarm[0]=1; exit;} //Ако въобще няма конвейор, продължавай да проверяваш (но след 1 стъпка, защото вече проверихме) в случай че играчът сложи такъв
  break;
 //---------------------------------------------------------------------------------------------------\\   
  case straight_path_down: 
      if instance_position(gridx,gridy+1,obj_conveyor)
        { 
          switch(instance_position(gridx,gridy+1,obj_conveyor).conveyor_type)
           {
            case down:
              path_start(straight_path_down,1,0,0);
              path = straight_path_down;
              exit;
            break;
            
            case right:
             switch(position)
              {
               case "center":
                 if not collision_rectangle(x-14,y+16,x+14,y+31,obj_item,0,1)   
                   {                    
                     y+=16;
                     path_start(straight_path_right_half,1,0,0);       
                     path = straight_path_right;  
                     position = "left";  
                     exit;
                    } else {alarm[0]=1; exit;}             
               break;
               
               case "left":
                 if not collision_rectangle(x-14+16,y+16,x+14+16,y+31,obj_item,0,1)   
                   {                    
                     y+=16;
                     path_start(straight_path_right_one_third,1,0,0);       
                     path = straight_path_right;  
                     position = "left";  
                     exit;
                    } else {alarm[0]=1; exit;}             
               break;
               
               case "right":
                 if not collision_rectangle(x-14-16,y+16,x+14-16,y+31,obj_item,0,1)   
                   {                    
                     y+=16;
                     path_start(straight_path_right_two_third,1,0,0);       
                     path = straight_path_right;  
                     position = "left";  
                     exit;
                    } else {alarm[0]=1; exit;}             
               break;
              }

 
            break;
            
            case left:
             switch(position)
              {
               case "center":
                if not collision_rectangle(x-14,y+16,x+14,y+31,obj_item,0,1)   
                   {
                     y+=16;
                     path_start(straight_path_left_half,1,0,0);       
                     path = straight_path_left;
                     position = "right";    
                     exit;
                   }
                  else {alarm[0]=1; exit;}                  
               break;
               
               case "left":
                if not collision_rectangle(x-14+16,y+16,x+14+16,y+31,obj_item,0,1)   
                   {
                     y+=16;
                     path_start(straight_path_left_two_third,1,0,0);       
                     path = straight_path_left;
                     position = "right";    
                     exit;
                   }  else {alarm[0]=1; exit;}                  
               break;
               
               case "right":
                if not collision_rectangle(x-14-16,y+16,x+14-16,y+31,obj_item,0,1)   
                   {
                     y+=16;
                     path_start(straight_path_left_one_third,1,0,0);       
                     path = straight_path_left;
                     position = "right";    
                     exit;
                   }  else {alarm[0]=1; exit;}                  
               break;
              }
     
            break;
            
            default:
               alarm[0]=1; exit;
               break;
           }
           
         } else {alarm[0]=1; exit;}
  break;
//---------------------------------------------------------------------------------------------------\\  
  case straight_path_up: 
      if instance_position(gridx,gridy-1,obj_conveyor)
        { 
          switch(instance_position(gridx,gridy-1,obj_conveyor).conveyor_type)
           {
            case up:
             path_start(straight_path_up,1,0,0);
             path = straight_path_up;
             exit;
            break;
            
            
            case right:
             switch(position)
              {
               case "center":
                if not collision_rectangle(x-14,y-16,x+14,y-31,obj_item,0,1)   
                   {
                     y-=16;
                     path_start(straight_path_right_half,1,0,0);       
                     path = straight_path_right;  
                     position = "right";
                     exit;  
                   } else {alarm[0]=1; exit;}    
               break;
               
               case "left":
                if not collision_rectangle(x-14-16,y-16,x+14-16,y-31,obj_item,0,1)   
                   {
                     y-=16;
                     path_start(straight_path_right_two_third,1,0,0);       
                     path = straight_path_right;  
                     position = "right";
                     exit;  
                   } else {alarm[0]=1; exit;}    
               break;
               
               case "right":
                if not collision_rectangle(x-14+16,y-16,x+14+16,y-31,obj_item,0,1)   
                   {
                     y-=16;
                     path_start(straight_path_right_one_third,1,0,0);       
                     path = straight_path_right;  
                     position = "right";
                     exit;  
                   } else {alarm[0]=1; exit;}    
               break;
              }
                 
            break;
            
            case left:
             switch(position)
              {
               case "center":
                if not collision_rectangle(x-14,y-16,x+14,y-31,obj_item,0,1)   
                     {
                       y-=16;
                       path_start(straight_path_left_half,1,0,0);       
                       path = straight_path_left;  
                       position = "left";
                       exit; 
                     } else {alarm[0]=1; exit;}                 
               break;
               
               case "left":
                if not collision_rectangle(x-14-16,y-16,x+14-16,y-31,obj_item,0,1)   
                     {
                       y-=16;
                       path_start(straight_path_left_one_third,1,0,0);       
                       path = straight_path_left;  
                       position = "left";
                       exit; 
                     } else {alarm[0]=1; exit;}                 
               break;
               
               case "right":
                if not collision_rectangle(x-14+16,y-16,x+14+16,y-31,obj_item,0,1)   
                     {
                       y-=16;
                       path_start(straight_path_left_two_third,1,0,0);       
                       path = straight_path_left;  
                       position = "left";
                       exit; 
                     } else {alarm[0]=1; exit;}                 
               break;
              }
      
            break;
            
            default:
               alarm[0]=1; exit;
               break;
           }
         }else{alarm[0]=1; exit;} 
  break;
 //---------------------------------------------------------------------------------------------------\\   

}

if !path_index {alarm[0] = 1;}