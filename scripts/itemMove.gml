///itemMove();
//Тази функция е същия код от obj_item > End of Path, но просто във функция

gridxx = floor(x/64)*64;
gridyy = floor(y/64)*64;

switch(path)
{
 //---------------------------------------------------------------------------------------------------\\ 
  case straight_path_right: // ако вървиме надясно
      if instance_position(x+1,y,obj_conveyor) //ако има конвейор от дясно
        { 
          switch(instance_position(x+1,y,obj_conveyor).conveyor_type) // и какъв е вида на конвейора
           {
            case right: // тука сочи надясно
             path_start(straight_path_right,1,0,0);
             path = straight_path_right;
             exit;
            break;
           //------------TURNS-----------\\ 
            case left_to_right:
             switch(position)
              {
               case "center":
                 path_start(turn_path_left_to_right_medium,1,0,0);
                 path = straight_path_right;
                 path_to_set = straight_path_down; alarm[2] = turn_medium;
                 exit;
               break;
               
               case "right":
                 path_start(turn_path_left_to_right_short,1,0,0);
                 path = straight_path_right;
                 path_to_set = straight_path_down; alarm[2] = turn_short;
                 exit;
               break;
               
               case "left":
                 path_start(turn_path_left_to_right_long,1,0,0);
                 path = straight_path_right;
                 path_to_set = straight_path_down; alarm[2] = turn_long;
                 exit;
               break;
              }             
            break;
            
            case left_to_up:
             switch(position)
              {
               case "center":
                 path_start(turn_path_left_to_right_medium,1,0,0);
                 path = straight_path_right;
                 path_to_set = straight_path_down; alarm[2] = turn_medium;
                 exit;
               break;
               
               case "right":
                 path_start(turn_path_left_to_right_short,1,0,0);
                 path = straight_path_right;
                 path_to_set = straight_path_down; alarm[2] = turn_short;
                 exit;
               break;
               
               case "left":
                 path_start(turn_path_left_to_right_long,1,0,0);
                 path = straight_path_right;
                 path_to_set = straight_path_down; alarm[2] = turn_long;
                 exit;
               break;
              }             
            break;           
           //------------TURNS END-----------\\         
           
               
            case down: //ако конвейора в който влиза сочи на долу 
              switch(position)//не отново...  позицията на предмета в конвейора, в коя "лента" е
               {
                case "center": // ако предмета се намира в центъра на конвейора
                  if not collision_rectangle(x+31,y-14,x+16,y+14,obj_item,0,1)//ако няма обект obj_item на конвейора в който влиза 
                   {
                        x+=2; alarm[1]=1; steps_to_move = 14;  moving_direction = right;
                        //path_start(straight_path_down_half,1,0,0);
                        path = straight_path_down; sub_path = "half down";
                        position = "right"; // вече се намира от дясната страна
                        exit; //излизаме от функцията (scripta) itemMove() защото направихме квото треа, няма нужда да проверяваме нататък
                   }   else {alarm[0] = 1; exit;} // еми няма място брат, изчакай малко да се освободи място на конвейора и провери пак след 1 стъпка
                break;
                
                case "right":  // ако се намира от дясната му страна (по посока на движението)
                  if not collision_rectangle(x+31,y-14,x+16,y+14,obj_item,0,1)//ако няма обект obj_item на конвейора в който влиза 
                   {
                        x+=2; alarm[1]=1; steps_to_move = 14;  moving_direction = right; 
                        //path_start(straight_path_down_one_third,1,0,0); 
                        path = straight_path_down; sub_path = "one third down";
                        position = "right";// вече се намира от дясната страна
                        exit; 
                   } else {alarm[0] = 1; exit;}                   
                break;
                
                case "left":   // и ако се намира от лявата страна
                  if not collision_rectangle(x+31,y-14,x+16,y+14,obj_item,0,1)//ако няма обект obj_item на конвейора в който влиза 
                   {
                        x+=2; alarm[1]=1; steps_to_move = 14;  moving_direction = right;
                        //path_start(straight_path_down_two_third,1,0,0); 
                        path = straight_path_down; sub_path = "two third down";
                        position = "right"; // вече се намира от дясната страна
                        exit; 
                   }   else {alarm[0] = 1; exit;}                         
                break;
               }
             
            
            
            case up: //айде отново, ако конвейора в който влиза сочи на горе
             switch(position)
              {
               case "center":
                 if not collision_rectangle(x+31,y-14,x+16,y+14,obj_item,0,1)//ако няма обект obj_item на конвейора в който влиза         
                   {
                     x+=2; alarm[1]=1; steps_to_move = 14;  moving_direction = right;  
                     //path_start(straight_path_up_half,1,0,0);
                     path = straight_path_up; sub_path = "half up";
                     position = "left"; // този сочи нагоре значи ще е от лявата му страна
                     exit; 
                   } else {alarm[0]=1; exit;} //няма място, навивам ти аларма да провериш пак след 1 стъпка (1 стъпка = 1 fps)                              
               break;
               
               case "right":
                 if not collision_rectangle(x+31,y-14,x+16,y+14,obj_item,0,1)//ако няма обект obj_item на конвейора в който влиза         
                   {
                     x+=2; alarm[1]=1; steps_to_move = 14;  moving_direction = right;  
                     //path_start(straight_path_up_two_third,1,0,0);
                     path = straight_path_up; sub_path = "two third up";
                     position = "left"; // този сочи нагоре значи ще е от лявата му страна
                     exit; 
                   } else {alarm[0]=1; exit;}                    
               break;
               
               case "left":
                 if not collision_rectangle(x+31,y-14,x+16,y+14,obj_item,0,1)//ако няма обект obj_item на конвейора в който влиза         
                   {
                     x+=2; alarm[1]=1; steps_to_move = 14;  moving_direction = right;  
                     //path_start(straight_path_up_one_third,1,0,0);
                     path = straight_path_up; sub_path = "one third up";
                     position = "left"; // този сочи нагоре значи ще е от лявата му страна
                     exit; 
                   } else {alarm[0]=1; exit;}                    
               break; 
              }
            break; 
            
            default:
               alarm[0]=1; exit;
            break;            
           }
         }else{alarm[0]=1; exit;} // не че няма място, ама то няма и конвейор хех, изчакай докато някой ти построй такъв
  break;
 //---------------------------------------------------------------------------------------------------\\ 
    case straight_path_left: 
      if instance_position(x-1,y,obj_conveyor)
        { 
          switch(instance_position(x-1,y,obj_conveyor).conveyor_type)
             {
               case left:
                 path_start(straight_path_left,1,0,0);
                 path = straight_path_left;
                 exit;
               break;
                        
                                                          
               case down: 
                 switch(position)
                  {
                   case "center":
                     if not collision_rectangle(x-31,y-14,x-16,y+14,obj_item,0,1)//ако няма обект obj_item на конвейора в който влиза         
                      {
                       x-=2; alarm[1]=1; steps_to_move = 14;  moving_direction = left;  
                       //path_start(straight_path_down_half,1,0,0);       
                       path = straight_path_down; sub_path = "half down"; 
                       position = "left";  
                       exit;
                      } else {alarm[0]=1; exit;}                    
                   break;
                   
                   case "right":
                     if not collision_rectangle(x-31,y-14,x-16,y+14,obj_item,0,1)//ако няма обект obj_item на конвейора в който влиза         
                      {
                       x-=2; alarm[1]=1; steps_to_move = 14;  moving_direction = left;  
                       //path_start(straight_path_down_two_third,1,0,0);       
                       path = straight_path_down; sub_path = "two third down";
                       position = "left";
                       exit;   
                      } else {alarm[0]=1; exit;}                      
                   break;
                   
                   case "left":
                     if not collision_rectangle(x-31,y-14,x-16,y+14,obj_item,0,1)//ако няма обект obj_item на конвейора в който влиза         
                      {
                       x-=2; alarm[1]=1; steps_to_move = 14;  moving_direction = left;  
                       //path_start(straight_path_down_one_third,1,0,0);       
                       path = straight_path_down; sub_path = "one third down";  
                       position = "left";
                       exit;   
                      } else {alarm[0]=1; exit;}                      
                   break;                   
                  }                     
               break;                
             
                                                         
               case up:
                 switch(position)
                  {
                   case "center":
                    if not collision_rectangle(x-31,y-14,x-16,y+14,obj_item,0,1)       
                     {
                      x-=2; alarm[1]=1; steps_to_move = 14;  moving_direction = left;  
                      //path_start(straight_path_up_half,1,0,0);       
                      path = straight_path_up; sub_path = "half up";
                      position = "right";
                      exit;    
                     } else {alarm[0]=1;exit;}                      
                   break;
                   
                   case "left":
                    if not collision_rectangle(x-31,y-14,x-16,y+14,obj_item,0,1)       
                     {
                      x-=2; alarm[1]=1; steps_to_move = 14;  moving_direction = left;  
                      //path_start(straight_path_up_two_third,1,0,0);       
                      path = straight_path_up; sub_path = "two third up";
                      position = "right";
                      exit;    
                     } else {alarm[0]=1;exit;}                      
                   break;
                   
                   case "right":
                    if not collision_rectangle(x-31,y-14,x-16,y+14,obj_item,0,1)       
                     {
                      x-=2; alarm[1]=1; steps_to_move = 14;  moving_direction = left;  
                      //path_start(straight_path_up_one_third,1,0,0);       
                      path = straight_path_up; sub_path = "one third up";
                      position = "right";
                      exit;    
                     } else {alarm[0]=1;exit;}                      
                   break;
                  }
  
               break;
               
               default:
               alarm[0]=1; exit;
               break;
         } 
      }else{alarm[0]=1; exit;} //Ако въобще няма конвейор, продължавай да проверяваш (но след 1 стъпка, защото вече проверихме) в случай че играчът сложи такъв
  break;
 //---------------------------------------------------------------------------------------------------\\   
  case straight_path_down: 
      if instance_position(x,y+1,obj_conveyor)
        { 
          switch(instance_position(x,y+1,obj_conveyor).conveyor_type)
           {
            case down:
              path_start(straight_path_down,1,0,0);
              path = straight_path_down;
              exit;
            break;
            
            case right:
             switch(position)
              {
               case "center":
                 if not collision_rectangle(x-13,y+16,x+14,y+31,obj_item,0,1)   
                   {                    
                     y+=2; alarm[1]=1; steps_to_move = 14;  moving_direction = down;  
                     //path_start(straight_path_right_half,1,0,0);       
                     path = straight_path_right; sub_path = "half right";  
                     position = "left";  
                     exit;
                    } else {alarm[0]=1; exit;}             
               break;
               
               case "left":
                 if not collision_rectangle(x-14,y+16,x+14,y+31,obj_item,0,1)   
                   {                    
                     y+=2; alarm[1]=1; steps_to_move = 14;  moving_direction = down;
                     //path_start(straight_path_right_one_third,1,0,0);       
                     path = straight_path_right; sub_path = "one third right";  
                     position = "left";  
                     exit;
                    } else {alarm[0]=1; exit;}             
               break;
               
               case "right":
                 if not collision_rectangle(x-14,y+16,x+14,y+31,obj_item,0,1)   
                   {                    
                     y+=2; alarm[1]=1; steps_to_move = 14;  moving_direction = down;
                     //path_start(straight_path_right_two_third,1,0,0);       
                     path = straight_path_right; sub_path = "two third right";  
                     position = "left";  
                     exit;
                    } else {alarm[0]=1; check = 8; exit;}             
               break;
              }

 
            break;
            
            case left:
             switch(position)
              {
               case "center":
                if not collision_rectangle(x-13,y+16,x+14,y+31,obj_item,0,1)   
                   {
                     y+=2; alarm[1]=1; steps_to_move = 14;  moving_direction = down;
                     //path_start(straight_path_left_half,1,0,0);       
                     path = straight_path_left; sub_path = "half left";
                     position = "right";    
                     exit;
                   }
                  else {alarm[0]=1; exit;}                  
               break;
               
               case "left":
                if not collision_rectangle(x-13,y+16,x+14,y+31,obj_item,0,1)   
                   {
                     y+=2; alarm[1]=1; steps_to_move = 14;  moving_direction =  down;
                     //path_start(straight_path_left_two_third,1,0,0);       
                     path = straight_path_left; sub_path = "two third left";
                     position = "right";    
                     exit;
                   }  else {alarm[0]=1; exit;}                  
               break;
               
               case "right":
                if not collision_rectangle(x-13-16,y+16,x+14,y+31,obj_item,0,1)   
                   {
                     y+=2; alarm[1]=1; steps_to_move = 14;  moving_direction = down;
                     //path_start(straight_path_left_one_third,1,0,0);       
                     path = straight_path_left; sub_path = "one third left";
                     position = "right";    
                     exit;
                   }  else {alarm[0]=1; exit;}                  
               break;
              }
     
            break;
            
            default:
               alarm[0]=1; exit;
               break;
           }
           
         } else {alarm[0]=1; exit;}
  break;
//---------------------------------------------------------------------------------------------------\\  
  case straight_path_up: 
      if instance_position(x,y-1,obj_conveyor)
        { 
          switch(instance_position(x,y-1,obj_conveyor).conveyor_type)
           {
            case up:
             path_start(straight_path_up,1,0,0);
             path = straight_path_up;
             exit;
            break;
            
            
            case right:
             switch(position)
              {
               case "center":
                if not collision_rectangle(x-13,y-16,x+14,y-31,obj_item,0,1)   
                   {
                     y-=2; alarm[1]=1; steps_to_move = 14;  moving_direction = up;
                     //path_start(straight_path_right_half,1,0,0);       
                     path = straight_path_right; sub_path = "half right";    
                     position = "right";
                     exit;  
                   } else {alarm[0]=1; exit;}    
               break;
               
               case "left":
                if not collision_rectangle(x-13,y-16,x+14,y-31,obj_item,0,1)   
                   {
                     y-=2; alarm[1]=1; steps_to_move = 14;  moving_direction = up;
                     //path_start(straight_path_right_two_third,1,0,0);       
                     path = straight_path_right; sub_path = "two third right";    
                     position = "right";
                     exit;  
                   } else {alarm[0]=1; exit;}    
               break;
               
               case "right":
                if not collision_rectangle(x-13,y-16,x+14,y-31,obj_item,0,1)   
                   {
                     y-=2; alarm[1]=1; steps_to_move = 14;  moving_direction = up;
                     //path_start(straight_path_right_one_third,1,0,0);       
                     path = straight_path_right; sub_path = "one third right";    
                     position = "right";
                     exit;  
                   } else {alarm[0]=1; exit;}    
               break;
              }
                 
            break;
            
            case left:
             switch(position)
              {
               case "center":
                if not collision_rectangle(x-13,y-16,x+14,y-31,obj_item,0,1)   
                     {
                       y-=2; alarm[1]=1; steps_to_move = 14;  moving_direction = up;
                       //path_start(straight_path_left_half,1,0,0);       
                       path = straight_path_left; sub_path = "half left";   
                       position = "left";
                       exit; 
                     } else {alarm[0]=1; exit;}                 
               break;
               
               case "left":
                if not collision_rectangle(x-13,y-16,x+14,y-31,obj_item,0,1)   
                     {
                       y-=2; alarm[1]=1; steps_to_move = 14;  moving_direction = up;
                       //path_start(straight_path_left_one_third,1,0,0);       
                       path = straight_path_left; sub_path = "one third left";     
                       position = "left";
                       exit; 
                     } else {alarm[0]=1; exit;}                 
               break;
               
               case "right":
                if not collision_rectangle(x-13,y-16,x+14,y-31,obj_item,0,1)   
                     {
                       y-=2; alarm[1]=1; steps_to_move = 14;  moving_direction = up;
                       //path_start(straight_path_left_two_third,1,0,0);       
                       path = straight_path_left; sub_path = "two third left";     
                       position = "left";
                       exit; 
                     } else {alarm[0]=1; exit;}                 
               break;
              }
      
            break;
            
            default:
               alarm[0]=1; exit;
               break;
           }
         }else{alarm[0]=1; exit;} 
  break;
 //---------------------------------------------------------------------------------------------------\\   

}

if !path_index {alarm[0] = 1;}
