

conveyor_type = argument0;
switch(conveyor_type)
 {
  case right:
   sprite_index = spr_conveyor_right;
   path = straight_path_right;

  break;
  
  case left:
   sprite_index = spr_conveyor_left;
   path = straight_path_left;

  break;
  
  case up:
   sprite_index = spr_conveyor_up;
   path = straight_path_up;
  break;  
    
  case down:
   sprite_index = spr_conveyor_down;
   path = straight_path_down;
  break; 
 }