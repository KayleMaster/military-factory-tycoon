       
        
         
var width = argument0; width = 1028;
var height = argument1;  height= 768;
           texture_set_interpolation(false);
            
        display_reset(0, 0); //not too surprising that resetting the display comes first. Only necessary if you give players the option to change aa and vsync in game



        window_set_size(width, height); //this is the only function that needs to run on a separate step. I've found it will not work correctly if used in the same step as the other functions



        window_set_fullscreen(false); //if setting it to fullscreen (true), window_set_size() won't have any effect.



        surface_resize(application_surface, width, height); //This will resize the application surface so that it fills the screen/window correctly. Only use this along with views.
        view_hview[0] = height; 
        view_wview[0] = width;
        view_hport[0] = height; 
        view_wport[0] = width; 
        display_set_gui_size(width, height); //resize the gui to fit the new resolution so any draw GUI events will appear in the correct place.


