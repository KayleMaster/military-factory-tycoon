///IsoToScreenY(LocalX,LocalY)
//Transform grid y coordinate to absolute screen y coordinate

return ((argument0 + argument1) * 64/2);
