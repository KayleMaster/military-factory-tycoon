///IsoToScreenX(LocalX,LocalY)
//Transform grid x coordinate to absolute screen x coordinate

return ((argument0 - argument1) * 128/2);
